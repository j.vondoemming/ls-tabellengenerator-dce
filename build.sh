#!/bin/bash

#IN="Choice-Profile.json"
#OUT="data-installation.json"
#NAME=installation
#NUMPADDING=2
IN="${1}"
OUT="${2}"
NAME="${3}"
NUMPADDING="${4}"
KEYSFILE="${5}"
HEADERFILE="${6}"

set -e

TMP_DIR="$(mktemp -d)"

mkdir -p builds

IMGJSON="$(cat images.json | jq '.')"

#NUM="$(( $(cat "${IN}" | jq '. | length') / 2 ))"
NUM="$(cat "${IN}" | jq '. | length')"


echo "NUM: $NUM"

#{
#	"Anzahl an Verbindungsabbrüchen" :  "https://survey.academiccloud.de/upload/surveys/339594/images/interruptions.png",
#	"Internetgeschwindigkeit mit VPN" : "https://survey.academiccloud.de/upload/surveys/339594/images/speed.png",
#	"Akkustand des Smartphones" :       "https://survey.academiccloud.de/upload/surveys/339594/images/plug.png",
#	"VPN Einschaltmethode" :            "https://survey.academiccloud.de/upload/surveys/339594/images/touch.png",
#	"Preis" :                           "https://survey.academiccloud.de/upload/surveys/339594/images/Euro_symbol.png",
#	"App Bewertung" :                   "https://survey.academiccloud.de/upload/surveys/339594/images/review.png",
#	"Anzahl an Downloads" :             "https://survey.academiccloud.de/upload/surveys/339594/images/download.png",
#	"Anbieterstandort" :                "https://survey.academiccloud.de/upload/surveys/339594/images/cloud.png"
#}
# {
#  "Choice-Satz" : 1,
#  "Choice-ID" : 1,
#  "Preis" : "kostenlos",
#  "App Bewertung" : "Gut (4 - 5 Sterne)",
#  "Anzahl an Downloads" : "10.000 - 100.000",
#  "In-app Werbung" : "ja",
#  "Anbieterstandort" : "USA"
# },
function add_keys() {
	local JSON="$(cat - | jq '.')"
	local KEYS=""
	if [ -f "${KEYSFILE}" ]; then
		KEYS="$(cat "${KEYSFILE}" | jq '.')"
	else
		KEYS="$(echo "$JSON" | grep -v 'Choice-' | jq '. | keys')"
	fi
	#local NUMKEYS="$(echo "$KEYS" | jq '. | length')"
	#echo "Adding Images" >&2
	echo "$JSON" > "${TMP_DIR}/value.json"
	printf '{"keys": %s}' "$KEYS" > "${TMP_DIR}/keys.json"
	jq -s '.[0] * .[1]' "${TMP_DIR}/value.json" "${TMP_DIR}/keys.json"
}

function gen_json() {

FIRST=t
COUNTER=1

printf '{'
for i in $(seq 0 2 $(( ${NUM} - 1)) ) ; do

	#echo "i: $i"
	CURA="$(cat "${IN}" | jq ".[${i}]" | add_keys)"
	CURB="$(cat "${IN}" | jq ".[$(( ${i} + 1 ))]" | add_keys)"

	if [ "$FIRST" = "t" ]; then
		FIRST=f
	else
		printf ',\n'
	fi

	printf '"%s": {' $COUNTER
	printf '"a": %s,\n' "${CURA}"
	printf '"b": %s\n' "${CURB}"
	#echo "${CURA}" | jq '.'
	#echo "${CURB}" | jq '.'
	printf '}\n'

	COUNTER=$(( $COUNTER + 1 ))

done
printf '}'

}

printf '{"cases": %s }' "$(gen_json | jq '.')"  | tee "${TMP_DIR}/d.json"
printf '{"images": %s }' "$(cat images.json | jq '.')"  | tee "${TMP_DIR}/images.json"
printf '{"header": "%s" }' "$(cat "${HEADERFILE}")"  | tee "${TMP_DIR}/header.json"

jq -s '.[0] * .[1] * .[2]' "${TMP_DIR}/d.json" "${TMP_DIR}/images.json" "${TMP_DIR}/header.json" | tee "${OUT}"

pushd builds
DCE_COUNT="$(cat "${TMP_DIR}/d.json" | jq '.["cases"] | length')"
DCE_JSON="../${OUT}"
TABLE_TMPL_FILE="../templates/table.html.j2"
for ((j = 1; j <= DCE_COUNT; j++)); do
	FILE="$(printf "%s_question_%0*d.html" "${NAME}" "${NUMPADDING}" "${j}")"
	echo "(${j}/${DCE_COUNT}) Builing ${FILE}"
	jinja2 --format=json --strict -D j="${j}" -D name="${NAME}" -o "${FILE}" "${TABLE_TMPL_FILE}" <(cat "${DCE_JSON}")
done
popd
