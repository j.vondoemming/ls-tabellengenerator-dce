default: build

doe:
	./doe.py

build:
	./build.sh "Choice-Profile_installation.json" "data-installation.json" installation 2 "./keys-installation.json" "./header-installation.txt"
	./build.sh "Choice-Profile_location_installation.json" "data-location_installation.json" location_installation 2 "./keys-installation.json" "./header-installation.txt"
	./build.sh "Choice-Profile_benutzung.json" "data-benutzung.json" benutzung 2 "./keys-benutzung.json" "./header-benutzung.txt"

reformat:
	./reformat.sh
