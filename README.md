# Limesurvey Tabellengenerator für Discrete Choice Experiment

[[_TOC_]]

## Abhängigkeiten

- GNU Coreutils
- Bash
- `jq`
- Python 3
- Python-pip 3
- GNU Make

### Pyhton

Installiere Python3 Abhängigkeiten mit:
```bash
pip3 install -r requirements.txt
```


## Klonen

```bash
git clone git@gitlab.gwdg.de:j.vondoemming/ls-tabellengenerator-dce.git
```

## Ausführen

```bash
make
```



## Lizenzhinweis

Das JavaScript in [templates/table.html.j2](templates/table.html.j2) basiert auf
Code von [diesem Forum Thread](https://forums.limesurvey.org/forum/can-i-do-this-with-limesurvey/121970-choice-experiment-conjoint-analysis).


Alles andere in diesem Repo, wo ich Lizenzrechte drauf habe, steht unter der
CC0 Lizenz.

